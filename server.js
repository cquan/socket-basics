var PORT = process.env.PORT || 3000;

var moment = require('moment');

var express = require('express');
var app = express();

//start a new server and use this express app as boilerplate
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.use(express.static( __dirname + '/public'));

//this obj is key-value pairs (socketId : userJointRoom)
var clientInfo = {};

// send current users to provided socket
function sendCurrentUsers(socket) {
    var info = clientInfo[socket.id];
    var users = [];

    if (typeof info === 'undefined') {
        return;
    }

    //takes an obj and return the array of all attributes in that Object
    Object.keys(clientInfo).forEach( (socketId)=>{
        var userInfo = clientInfo[socketId];

        if (info.room === userInfo.room) {
            users.push(userInfo.name);
        }
    });
    //emit the message for all users found
    socket.emit('message', {
        name: 'System',
        text: 'Current users: ' + users.join(','),
        //join can put all elements into one string with , separating them
        timestamp: moment().valueOf()
    });
}

//let socket.io listen to events
io.on('connection', (socket)=>{ //socket is the indevidual connection
    console.log('User connected via socket.io!');

    //disconnect is a built in socket io event
    socket.on('disconnect', ()=>{
        var userData = clientInfo[socket.id];
        if ( typeof userData !== undefined ) {
            socket.leave(userData.room);
            io.to(userData.room).emit('message', {
                name: 'System',
                text: userData.name + ' has left!',
                timestamp: moment().valueOf()
            });
            //delete an attribute from an object
            delete clientInfo[socket.id];
        }
    });

    socket.on('joinRoom', (req)=>{
        //[] used here can dynamically set the attribute of clientInfo
        clientInfo[socket.id] = req;
        //adds the sockets to the room and fires an optinoally callback
        socket.join(req.room);
        socket.broadcast.to(req.room).emit('message', {
            name: 'System',
            text: req.name + ' has joined!',
            timestamp: moment().valueOf()
        });
    });

    socket.on('message', (message)=>{
        console.log("message received " + message.text);

        if (message.text === '@currentUsers') {
            sendCurrentUsers(socket);
        }else{
            //add the timestamp here in javascript timestamp (milliseconds)
            message.timestamp = moment().valueOf();
            //send the message to EVERYONE
            //the `to` method only emit the message the to sockets in given room
            io.to(clientInfo[socket.id].room).emit('message', message);
        }
    });

    //send an object on `message` event
    socket.emit('message', {
        name: 'System',
        text: 'Welcome to the chat application',
        //add a timestamp here
        timestamp: moment().valueOf()
    });
});

http.listen(PORT, function(){
    console.log('Server started!');
});

