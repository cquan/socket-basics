#README
This project provides a simplet chatting service where user can input their username and chat room name and chat together. The finished UI is shown below:


The highlights of this prject includes:
* using `socket.io` for real time chatting experience
* using `jquery` for simple `DOM` manipulation
* using `moment.js` for converting global time to local time
* using `bootstrap.css` for simple styling
